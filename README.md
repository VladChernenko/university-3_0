<div align="center">

# <b>Multicurrency cryptowallet(University project)</b>

<img src="https://images.ctfassets.net/q33z48p65a6w/2fi3pKDcUs07RnA0XXwh8S/53809dd161bbf81fdbb7bdd9200ae9b4/2204_Crypto-Educational-Email-4-1920x1080-EN.png?w=1200&h=645&fit=thumb">

</div>

## Intro

This is the web wallet based on <code>NodeJS/React</code> to allow you to run it on localhost and get access to multiple proejcts. You can make payments, call smart contracts and so on.

## Features

1. Multi currencies support
2. Payments over TOR
3. More in future versions