import express from 'express';

import algorand from '@klyntar/valardohaeris/algorand/vd.js'
import eth_like from '@klyntar/valardohaeris/eth_like/vd.js'
import solana from '@klyntar/valardohaeris/solana/vd.js'
import xrp from '@klyntar/valardohaeris/xrp/vd.js'
import fs from 'fs'


import Web3 from 'web3'

//To store accounts
import level from 'level'

const database = level('ACCOUNTS',{valueEncoding:'json'})


const app = express()
const port = 4000

const web3 = new Web3()


import bodyParser from 'body-parser'

import path from 'path'


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))


import sendAvaxTransaction from '../src/utils/sendAvax.js'







//_____________________________________ SERVER _____________________________________



app.post('/generate',async(req,res) => {

    let {id,coin} = req.body

    if(id===''){

        res.send('Please, enter the valid alias for key')

        return

    }

    let isExists = await database.get(id).catch(_=>false)

    
    if(isExists){

        res.send(`Alias already exists`)

        return

    }

    if(coin === 'Ethereum'){

        let keypair = await eth_like.generate()

        await database.put(id,keypair).catch(_=>false)

        res.send(keypair.address)

    }
    else if(coin === 'Avalanche'){

        let keypair = await eth_like.generate()

        await database.put(id,keypair).catch(_=>false)

        res.send(keypair.address)

    }
    else if(coin === 'Solana'){
        
        let keypair = await solana.generate()

        await database.put(id,keypair).catch(_=>false)

        res.send(keypair.address)

    }
    else if(coin === 'Algorand'){
        
        let keypair = await algorand.generate()

        await database.put(id,keypair).catch(_=>false)

        res.send(keypair.acc.addr)

    }
    else if(coin === 'XRP'){
        
        let keypair = await xrp.generate()

        await database.put(id,keypair).catch(_=>false)

        res.send(keypair.address)

    }else{

        res.send('Key is not present in local list')

    }

})

app.get('/export_accounts',async(req,res) => {
    
    let accounts = {}

    await new Promise(resolve=>

        database.createReadStream().on('data',data=>accounts[data.key]=data.value).on('close',resolve)

    ).catch(_=>{})
    
    fs.writeFileSync(path.resolve('accounts.bin'),JSON.stringify(accounts))    

    res.sendFile(path.resolve('accounts.bin'))
 
})


app.get('/my_accounts',async(req,res) => {

    res.setHeader('Access-Control-Allow-Origin','*')
  
    let accounts = []

    await new Promise(resolve=>

        database.createKeyStream().on('data',key=>accounts.push(key)).on('close',resolve)

    ).catch(_=>{})
    
    res.send(JSON.stringify(accounts))
 
})

app.post('/import_account',async(req,res) => {

    let {key,value} = req.body

    try{

        let account = web3.eth.accounts.privateKeyToAccount(value)

        let keypair = {address:account.address,privateKey:account.privateKey}
  
        // ETH only
        await database.put(key,keypair).catch(_=>false)
    
        res.send('OK')


    }catch(e){

        res.send('Error: '+e)

    }
     

})


app.post('/send_tx',async(req,res) => {

    let account = await database.get(req.body.selectedAccount)

    let receipt = await sendAvaxTransaction(req.body,account)

    res.send(JSON.stringify({
    
        status:'OK',
    
        msg:`Success => Tx hash is ${receipt.transactionHash}`
    
    }))
 
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})