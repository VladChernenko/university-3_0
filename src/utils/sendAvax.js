import Web3 from 'web3'
import Common from 'ethereumjs-common'
import pkg from '@ethereumjs/tx';
const {FeeMarketEIP1559Transaction} = pkg;


//AVALANCHE C-chain(EVM)
const web3 = new Web3('https://api.avax-test.network/ext/bc/C/rpc')

// new Web3.providers.HttpProvider('host',{agent:<agent with proxy(TOR,SOCKS,etc.)>})



export default async(txObject,account) => {

    let address=account.address

    let privateKey1 = Buffer.from(account.privateKey.slice(2),'hex')



    //__________________________________________________________START MAIN PART___________________________________________________________


    let txCount = await web3.eth.getTransactionCount(address)

       //Note-choose "scope"(Ropsten testnet in this case)
       let chain = Common.default.forCustomChain ('mainnet',{networkId:43113,chainId:43113,hardfork:'london'})

       // Build a transaction
       const rawTx = {
           "to"                    :   txObject.sendTo,
           "gasLimit"              :   web3.utils.toHex(70000),
           "maxFeePerGas"          :   web3.utils.toHex(web3.utils.toWei( txObject.sendFee+'' , 'gwei' ) ),
           "maxPriorityFeePerGas"  :   web3.utils.toHex(web3.utils.toWei( '27' , 'gwei' ) ),
           "value"                 :   web3.utils.toHex(web3.utils.toWei(txObject.amount+'','ether')),
           "data"                  :   web3.utils.toHex('Hello Web Project'),
           "nonce"                 :   web3.utils.toHex(txCount),
           "chainId"               :   web3.utils.toHex(43113),
           "accessList"            :   [],
           "type"                  :   "0x02"
       };
   
       // Creating a new transaction
       const tx = FeeMarketEIP1559Transaction.fromTxData( rawTx , { chain } );
   
   
   
   
   
   
       //Sign the transaction
       let signedTX=tx.sign(privateKey1)
            
       let raw = '0x' + signedTX.serialize().toString('hex')
   
       console.log('Transaction ———> ',raw)
   
       //Broadcast the transaction
       let receipt = await web3.eth.sendSignedTransaction(raw)
   
       return receipt
   

}