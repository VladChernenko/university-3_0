import '../../styles/for_generation.css'
import Select from 'react-select'
import {Component} from 'react'



const options = [

    { value: 'Ethereum', label:'Ethereum' },
    { value: 'Avalanche', label:'Avalanche' },
    { value: 'Solana', label:'Solana' },
    { value: 'Algorand', label:'Algorand'},
    { value: 'Tron', label:'Tron'},
    { value: 'XRP', label:'XRP'}
]



export default class GeneratePage extends Component {


    state = {
    
        selectedOption: null,

        address:null
    
    };
    
    handleChange = async selectedOption => {

        let futureState = {selectedOption,address:null}

        let keyid = prompt('Enter the alias for key:')
        
        let address = await fetch(`/generate`,{

            method:'POST',

            headers:{
                'content-type': 'application/json'
            },

            body:JSON.stringify({
            
                id:keyid,

                coin:selectedOption.value
            
            })

        }).then(r=>r.text()).catch(_=>false)

        futureState.address = address

        //Update the state
        this.setState(futureState)
    
    };

    render(){

        return (
      
            <div id="accounts_generator">
    
                <Select options={options} onChange={this.handleChange}/>
                
                <h3>Your address is:</h3>
                
                <pre>{this.state.address}</pre>

            </div>
        
        );    

    }

}