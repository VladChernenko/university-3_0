import {Component} from 'react'

import Select from 'react-select'


let selectedAccount = null

let amount = null

let sendTo = null

let sendFee = null


export default class Send extends Component {

  state = {

    options:[]

  }

  selectAccount = selectedOption => {

    selectedAccount = selectedOption.value

  }

  changeAmount = newValue => {

    amount = +newValue.target.value

  }

  changeTo = newValue => {

    sendTo = newValue.target.value

  }

  changeFee = newValue => {

    sendFee = +newValue.target.value

  }

  sendTransaction=async()=>{

    console.log('Going to send')

    console.log(selectedAccount)
    console.log(sendTo)
    console.log(sendFee)
    console.log(amount)

    let tx = {
      selectedAccount,sendTo,sendFee,amount
    }

    await fetch(`/send_tx`,{

        method:'POST',

        headers:{
            'content-type': 'application/json'
        },

        body:JSON.stringify(tx)

    }).then(r=>r.json()).then(resp=>{

      if(resp.status === 'OK') alert(resp.msg)

    }).catch(_=>false)


  }


    render() {

      fetch('http://localhost:4000/my_accounts').then(r=>r.json()).then(
  
        array_of_accounts => {
          
          let accounts = array_of_accounts.map(x=>({value:x,label:x}))

          this.setState({options:accounts})

        }
        
      )

        return (
            
            <div id="send_form">

                <h2>Choose what do you want to send - currency and account</h2>

                <div id="send_form_inner">

                    <Select options={this.state.options} onChange={this.selectAccount} placeholder="Select account"/>

                    <div id="input_send_block">

                    <div id="send_amount_block">
            
            <label for="send_amount">Amount <input id="send_amount" type="text" onChange={this.changeAmount}/></label>

          </div>

          <div>
  
            <label for="send_to">To <input id="send_to" type="text" onChange={this.changeTo}/></label>

          </div>

          <div>
  
              <label for="send_fee">Fee <input id="send_fee" type="text" onChange={this.changeFee}/></label>

          </div>

                    </div>

                <button onClick={this.sendTransaction}>Send!</button>

                </div>
               
        </div>

        )

    }
    
}