import {Component} from 'react'


export default class SettingsPage extends Component {

  state = {
    
    torEnabled:localStorage.getItem('TOR'),

    importValue:''

  }

  handleChange = _ => {

    let valueFromStorage = localStorage.getItem('TOR')

    if(valueFromStorage === 'ON'){

      localStorage.setItem('TOR','OFF')

      this.setState({...this.state,torEnabled:'OFF'})

    }else {

      localStorage.setItem('TOR','ON')

      this.setState({...this.state,torEnabled:'ON'})

    }
  
  }


  inputChange = newValue => {

    this.setState({...this.state,importValue:newValue.target.value})

  }

  makeImport = () => {

    console.log('State ',this.state)

    let [key,value] = this.state.importValue.split(":")

    fetch('/import_account',{

        method:'POST',
        body:JSON.stringify({key,value}),
        
        headers:{
          'content-type': 'application/json'
        }

    }).then(r=>r.text()).then(pureResponse=>{

      if(pureResponse==='OK'){

        alert('Account imported!')

        this.setState({...this.state,importValue:''})

      }

      else alert('Some error occured')

    }).catch(e=>alert(`Some error occured => ${e}`))

  }


  render(){

    return (
      <div id="settings_page">
        
        <div id="settings_wrapper">

          <div id="tor_togler">
            
            <p>Enable TOR</p>

            <input id="tor_checkbox" type="checkbox" checked={this.state.torEnabled==='ON'} onChange={this.handleChange}/>

          </div>
          
          <div>
            
            <p>Import account</p>
            
            <input id="import_account" type="text" placeholder='Key:Value' onChange={this.inputChange} value={this.state.importValue}/>

            <button onClick={this.makeImport}>Import</button>

          </div>
          
          <div id="export_accounts">
            
            <p>Export accounts</p>

            <button><a id="download_accounts" href="http://localhost:4000/export_accounts">Download</a></button>

          </div>
          

        </div>

      </div>
    );

  }

}