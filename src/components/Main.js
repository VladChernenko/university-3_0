//Components
import {Component} from 'react'


// For SPA
import {BrowserRouter,Route,Routes,Link} from "react-router-dom";



//Styles
import '../styles/for_arrows.scss'
import '../styles/cyber.scss'

//______________________Components______________________

// Default
import SocialsWidgets from './SocialsWidgets';

// Navigation
import NotFoundPage from './Navigation/NotFound';
import Settings from './Navigation/Settings';
import GeneratePage from './Navigation/Generate';
import Send from './Navigation/Send';







export class Main extends Component {

    render() {

        return (

            <BrowserRouter>

            <header>

                <div>
                
                    <div id="app">

    	                <div id="wrapper">

	    	                <h1 className={"glitch"} data-text="X-wallet">X-wallet</h1>
	            
                            <div className="sub">
                                The future stArts here
                            </div>

                            <div className="arrow">
                                <div className="chevron"></div>
                                <div className="chevron"></div>
                                <div className="chevron"></div>
                            </div>
               
                        </div>

                    </div>

                </div>
                
                <SocialsWidgets/>

                {/* Our selector */}

               <nav id="menu_selector">
                    <ul>
                        <li>
                            <Link to="/">Send</Link>
                        </li>
                        
                        <li>
                            <Link to="/generate">Generate</Link>
                        </li>

                        <li>
                            <Link to="/settings">Settings</Link>
                        </li>
                    
                    </ul>
                </nav>
        
            
            </header>


            <Routes>
            
                <Route path="/" element={<Send/>}/>
                <Route path="/send" element={<Send/>}/>

                <Route path="/generate" element={<GeneratePage/>}/>
                <Route path="/settings" element={<Settings/>}/>

                <Route path="/*" element={<NotFoundPage/>}/>
            
            </Routes>


            </BrowserRouter>
        
        )

    }
    
}